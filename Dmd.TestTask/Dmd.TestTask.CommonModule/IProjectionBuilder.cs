﻿namespace Dmd.TestTask.CommonModule
{
    public interface IProjectionBuilder
    {
        TProjection Build<TValue, TProjection>(TValue value);
        TValue Patch<TValue, TProjection>(TProjection projection, TValue value);
    }
}
