﻿using System.Threading.Tasks;

namespace Dmd.TestTask.Dal
{
    public interface IUnitOfWork
    {
        TestTaskContext Context { get; }
        
        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}
