﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Dmd.TestTask.Dal.ContextFactory
{
    public class ConfigurationContextDesignTimeFactory : IDesignTimeDbContextFactory<TestTaskContext>
    {
        public TestTaskContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<TestTaskContext> builder = new DbContextOptionsBuilder<TestTaskContext>();
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            builder.UseSqlServer(configuration.GetSection("ConnectionString").Value);
            return new TestTaskContext(builder.Options);
        }
    }
}
