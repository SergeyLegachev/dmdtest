﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dmd.TestTask.Dal.Migrations
{
    public partial class addClosedDateField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ClosedDate",
                table: "Goals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClosedDate",
                table: "Goals");
        }
    }
}
