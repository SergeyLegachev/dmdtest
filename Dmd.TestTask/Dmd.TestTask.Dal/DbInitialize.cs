﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Dmd.TestTask.Dal
{
    public class DbInitialize
    {
        private readonly TestTaskContext _appContext;

        public DbInitialize(TestTaskContext appContext)
        {
            _appContext = appContext ?? throw new ArgumentNullException(nameof(appContext));
        }

        public void Initialize()
        {
            _appContext.Database.Migrate();
        }
    }
}
