﻿using Dmd.TestTask.Dal.Repository.Abstract;
using Dmd.TestTask.Dal.Repository.Base;
using Dmd.TestTask.Domain.Entity;

namespace Dmd.TestTask.Dal.Repository
{
    public interface IGoalRepository : IRepository<Goal>
    {

    }

    public class GoalRepository : Repository<Goal>, IGoalRepository
    {
        public GoalRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
