﻿using System;
using System.Linq;
using Dmd.TestTask.Domain.Entity.Abstract;

namespace Dmd.TestTask.Dal.Repository.Abstract
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> Query(bool disableTracking = true);
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        void Remove(Guid id);
    }
}
