﻿using System;
using System.Linq;
using Dmd.TestTask.Dal.Repository.Abstract;
using Dmd.TestTask.Domain.Entity.Abstract;
using Microsoft.EntityFrameworkCore;

namespace Dmd.TestTask.Dal.Repository.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly IUnitOfWork _uow;

        public Repository(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
        }
        protected TestTaskContext DbContext => _uow.Context;
        protected DbSet<TEntity> Set => DbContext.Set<TEntity>();
        public IQueryable<TEntity> Query(bool disableTracking = true)
        {
            IQueryable<TEntity> query = Set.AsQueryable();

            if (disableTracking)
            {
                query = query.AsNoTracking();
            }
            return query;
        }

        public TEntity Insert(TEntity entity)
        {
            TEntity elem = Set.Add(entity) as TEntity;
            return DbContext.Entry(entity).Entity;
        }

        public TEntity Update(TEntity entity)
        {
            AttachIfNot(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Remove(Guid id)
        {
            TEntity elem = Set.FirstOrDefault(e => e.Id == id);
            if (elem == null)
            {
                throw new Exception("Item not found!");
            }
            AttachIfNot(elem);
            Set.Remove(elem);
        }

        protected virtual void AttachIfNot(TEntity entity)
        {
            if (!Set.Local.Contains(entity))
            {
                Set.Attach(entity);
            }
        }
    }
}
