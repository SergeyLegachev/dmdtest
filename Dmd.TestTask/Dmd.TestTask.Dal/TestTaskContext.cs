﻿using Dmd.TestTask.Dal.Mapping;
using Dmd.TestTask.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Dmd.TestTask.Dal
{
    public class TestTaskContext : DbContext
    {
        public TestTaskContext(DbContextOptions<TestTaskContext> options) : base(options)
        {

        }
        public virtual DbSet<Goal> Goals { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GoalMapping());
        }
    }
}
