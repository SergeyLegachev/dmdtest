﻿using System;
using System.Threading.Tasks;


namespace Dmd.TestTask.Dal
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestTaskContext _context;

        public UnitOfWork(TestTaskContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public TestTaskContext Context  => _context;
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }
    }
}
