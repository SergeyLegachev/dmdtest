﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Dmd.TestTask.Dal;
using Dmd.TestTask.Dal.Repository.Abstract;
using Dmd.TestTask.Domain.Entity.Abstract;

namespace Dmd.TestTask.Service.Services.Abstract
{
    public class Service<TEntity> : IService<TEntity> where TEntity : Entity
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IRepository<TEntity> _repository;

        public Service(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork)); ;
            _repository = repository ?? throw new ArgumentNullException(nameof(repository)); ;
        }

        public TEntity Update(TEntity entity)
        {
            _repository.Update(entity);
            _unitOfWork.SaveChanges();
            return entity;
        }

        public Task<int> DeleteAsync(Guid id)
        {
            _repository.Remove(id);
            return _unitOfWork.SaveChangesAsync();
        }

        public IQueryable<TEntity> Query(bool disableTracking = true)
        {
            return _repository.Query();
        }
    }
}
