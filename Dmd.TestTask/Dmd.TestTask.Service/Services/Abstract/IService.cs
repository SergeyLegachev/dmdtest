﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dmd.TestTask.Service.Services.Abstract
{
    public interface IService<TEntity>
    {
        TEntity Update(TEntity entity);

        Task<int> DeleteAsync(Guid id);

        IQueryable<TEntity> Query(bool disableTracking = true);
    }
}
