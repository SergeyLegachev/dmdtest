﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Dto;
using Dmd.TestTask.Service.Services.Abstract;

namespace Dmd.TestTask.Service.Services.Contract
{
    public interface IGoalService : IService<Goal>
    {
        Task<List<Goal>> GetAll();
        Task<Goal> GetByIdAsync(Guid id);
        Task<Goal> CreateAsync(Guid? parentId);
        Task<EditTaskDto> GetEditable(Guid id);
        Task Edit(EditTaskDto data);
    }
}
