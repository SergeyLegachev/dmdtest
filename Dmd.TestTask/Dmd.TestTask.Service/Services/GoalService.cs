﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dmd.TestTask.CommonModule;
using Dmd.TestTask.Dal;
using Dmd.TestTask.Dal.Repository;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Dto;
using Dmd.TestTask.Service.Exceptions;
using Dmd.TestTask.Service.Extensions;
using Dmd.TestTask.Service.Services.Abstract;
using Dmd.TestTask.Service.Services.Contract;
using Microsoft.EntityFrameworkCore;

namespace Dmd.TestTask.Service.Services
{
    public class GoalService : Service<Goal>, IGoalService
    {
        private readonly IProjectionBuilder _projectionBuilder;

        public GoalService(IUnitOfWork unitOfWork, IGoalRepository repository, IProjectionBuilder projectionBuilder) : base(unitOfWork, repository)
        {
            _projectionBuilder = projectionBuilder;
        }

        public async Task<List<Goal>> GetAll()
        {
            List<Goal> goals = await Query().Include(e => e.Parent)
                .Include(i => i.Children)
                .ToAsyncEnumerable()
                .Where(x => x.Parent == null)
                .ToList();
            return goals;
        }

        public async Task<Goal> GetByIdAsync(Guid id)
        {
            List<Goal> result = await Query()
                .Include(i => i.Children).ToAsyncEnumerable()
                .Where(x => x.Id == id).ToList();
            return result.FirstOrDefault();
        }

        public async Task<Goal> CreateAsync(Guid? parentId)
        {
            Goal parent = Query().FirstOrDefault(e => e.Id == parentId);
            int count = Query().Count();
            Goal newItem = new Goal
            {
                Status = Status.Assigned,
                ParentId = parentId,
                Assignee = string.Empty,
                Name = $"TBL{count}",
                InitiateDate = DateTime.UtcNow,
                Description = string.Empty
            };
            _repository.Insert(newItem);
            await _unitOfWork.SaveChangesAsync();
            return newItem;
        }

        private IEnumerable<ChildGoalDto> GetChildrenAsFlatList(IEnumerable<Goal> children)
        {
            IEnumerable<Goal> result = children.Traverse(task => task.Children);
            IEnumerable<ChildGoalDto> childTasks = result.Select(e => new ChildGoalDto
            {
                PlanDurationTitle = $"{e.PlanDuration / 3600}:{e.PlanDuration / 60 % 60}",
                FactDurationTitle = $"{e.FactDuration / 3600}:{e.FactDuration / 60 % 60}",
                Name = e.Name,
                Duration = e.PlanDuration,
                FactDuration = e.FactDuration,
                Status = e.Status,
                StatusText = e.Status.GetDescription()
            });
            return childTasks;
        }

        public async Task<EditTaskDto> GetEditable(Guid id)
        {
            var item = await GetByIdAsync(id);

            IEnumerable<ChildGoalDto> children = GetChildrenAsFlatList(item.Children.AsEnumerable());
            int childFactDuration = (children.Sum(e => e.FactDuration));
            int childPlanDuration = (children.Sum(e => e.Duration));
            EditTaskDto result = new EditTaskDto(item, children, childFactDuration, childPlanDuration);
            return result;
        }

        public async Task Edit(EditTaskDto data)
        {
            var item = await GetByIdAsync(data.Id);
            IEnumerable<Goal> children = item.Children.Traverse(task => task.Children);
            
            var mappedItem = _projectionBuilder.Patch(data, item);
            if (item.Status == Status.Finished)
            {
                //FR005(1)
                if (children.Any(e => e.Status == Status.Assigned))
                {
                    throw new BusinessRulesException("Не все подзадачи завершены!");
                }
                if (mappedItem.ClosedDate == null)
                {
                    throw new BusinessRulesException("Не указана дата завершения задачи!");
                }

                foreach (var el in children)
                {
                    el.Status = Status.Finished;
                    _repository.Update(el);
                }
            }
            _repository.Update(mappedItem);
            int count = await _unitOfWork.SaveChangesAsync();
        }
    }
}
