﻿using System;

namespace Dmd.TestTask.Service.Exceptions
{
    public interface IBusinessRulesException
    {

    }

    public class BusinessRulesException : Exception, IBusinessRulesException
    {
        public BusinessRulesException()
        {

        }

        public BusinessRulesException(string message)
            : base(message)
        {
        }

        public BusinessRulesException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
