﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dmd.TestTask.Domain.Entity;

namespace Dmd.TestTask.Service.Extensions
{
    public static class GoalChildrenExtension
    {
        public static IEnumerable<Goal> Traverse(this IEnumerable<Goal> items,
            Func<Goal, IEnumerable<Goal>> childSelector)
        {
            Stack<Goal> stack = new Stack<Goal>(items);
            while (stack.Any())
            {
                Goal next = stack.Pop();
                yield return next;
                foreach (Goal child in childSelector(next))
                    stack.Push(child);
            }
        }
    }
}
