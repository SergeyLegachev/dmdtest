﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dmd.TestTask.Domain.Entity
{
    public class Goal : Abstract.Entity
    {
        public virtual Goal Parent { get; set; }
        public Guid? ParentId { get; set; }
        public virtual ICollection<Goal> Children { get; set; } = new HashSet<Goal>();
        public string Description { get; set; }
        public string Assignee { get; set; }
        public Status Status { get; set; }
        public string Name { get; set; }
        public DateTime InitiateDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int PlanDuration { get; set; }
        public int FactDuration { get; set; }
    }

    public enum Status
    {
        [Description("Назначена")]
        Assigned = 1,
        [Description("Выполняется")]
        InAction = 2,
        [Description("Приостановлена")]
        Stopped = 3,
        [Description("Завершена ")]
        Finished = 4
    }
}
