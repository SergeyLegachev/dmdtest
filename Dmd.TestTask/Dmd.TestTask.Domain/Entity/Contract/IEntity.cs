﻿using System;

namespace Dmd.TestTask.Domain.Entity.Contract
{
    public interface IEntity : IEquatable<IEntity>
    {
        Guid Id { get; }
        
    }
}
