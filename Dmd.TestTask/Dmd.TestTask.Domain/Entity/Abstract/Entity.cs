﻿using System;
using System.Collections.Generic;
using Dmd.TestTask.Domain.Entity.Contract;


namespace Dmd.TestTask.Domain.Entity.Abstract
{
    public abstract class Entity : IEntity
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; protected set; }

        public bool Equals(IEntity obj)
        {
            return obj is Entity identity && Id.Equals(identity.Id);
        }
        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
        }
    }
}
