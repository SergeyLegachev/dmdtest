﻿using Dmd.TestTask.Domain.Entity;

namespace Dmd.TestTask.Dto
{
    public class ChildGoalDto
    {
        public string Name { get; set; }
        public string PlanDurationTitle { get; set; }
        public string FactDurationTitle { get; set; }
        public int Duration { get; set; }
        public int FactDuration { get; set; }
        public Status Status { get; set; }
        public string StatusText { get; set; }
    }
}
