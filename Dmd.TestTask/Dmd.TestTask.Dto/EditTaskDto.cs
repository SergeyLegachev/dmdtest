﻿using System;
using System.Collections.Generic;
using Dmd.TestTask.Domain.Entity;

namespace Dmd.TestTask.Dto
{
    public class EditTaskDto
    {
        public EditTaskDto() { }
        public EditTaskDto(Goal goal, IEnumerable<ChildGoalDto> children, int childFactDuration, int childPlanDuration)
        {
            Id = goal.Id;
            TaskHours = goal.PlanDuration / 3600;
            TaskMinutes = goal.PlanDuration / 60 % 60;
            FactTaskHours = goal.FactDuration / 3600;
            FactTaskMinutes = goal.FactDuration / 60 % 60;
            Name = goal.Name;
            ChildFactDuration = childFactDuration;
            ChildPlanDuration = childPlanDuration;
            Assignee = goal.Assignee;
            InitiateDate = goal.InitiateDate;
            ClosedDate = goal.ClosedDate;
            Description = goal.Description;
            ParentId = goal.ParentId;
            Status = goal.Status;
            Children = children;
            FactDuration =
                $"{(childFactDuration + goal.FactDuration) / 3600}:{(childFactDuration + goal.FactDuration) / 60 % 60}";
            PlanDuration =
                $"{(childPlanDuration + goal.PlanDuration) / 3600}:{(childPlanDuration + goal.PlanDuration) / 60 % 60}";
        }
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public string Assignee { get; set; }
        public Status Status { get; set; }
        public IEnumerable<ChildGoalDto> Children { get; set; }
        public string Name { get; set; }
        public DateTime InitiateDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int TaskHours { get; set; }
        public int TaskMinutes { get; set; }
        public int FactTaskHours { get; set; }
        public int FactTaskMinutes { get; set; }
        public string PlanDuration { get; set; }
        public string FactDuration { get; set; }
        public int ChildFactDuration { get; set; }
        public int ChildPlanDuration { get; set; }
    }
}
