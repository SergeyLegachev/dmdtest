﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Dmd.TestTask.CommonModule;
using Dmd.TestTask.Dto;
using Dmd.TestTask.Service.Services.Contract;
using Microsoft.AspNetCore.Mvc;
using Dmd.TestTask.Web.Models;

namespace Dmd.TestTask.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGoalService _goalService;
        private readonly IProjectionBuilder _projectionBuilder;

        public HomeController(IGoalService goalService, IProjectionBuilder projectionBuilder)
        {
            _goalService = goalService;
            _projectionBuilder = projectionBuilder;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<ActionResult> GoalInfo(Guid id)
        {
            EditTaskDto res = await _goalService.GetEditable(id);
            GoalEditViewModel goalEditViewModel = _projectionBuilder.Build<EditTaskDto, GoalEditViewModel>(res);
            return PartialView("~/Views/Home/Partials/GoalInfo.cshtml", goalEditViewModel);
        }

        [HttpGet("Tree")]
        public ActionResult Tree()
        {
            return PartialView("~/Views/Home/Partials/GoalPartial.cshtml");
        }

        [HttpPost("Edit")]
        public async Task<ActionResult> Edit(GoalEditViewModel model)
        {
            EditTaskDto editTaskDto = _projectionBuilder.Build<GoalEditViewModel, EditTaskDto>(model);
            await _goalService.Edit(editTaskDto);
            return RedirectToAction("GoalInfo", "Home", new {id = editTaskDto.Id});
        }
    }
}
