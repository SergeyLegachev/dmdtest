﻿using System;
using System.Threading.Tasks;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Service.Services.Contract;
using Microsoft.AspNetCore.Mvc;

namespace Dmd.TestTask.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GoalController : ControllerBase
    {
        private readonly IGoalService _goalService;

        public GoalController(IGoalService goalService)
        {
            _goalService = goalService;
        }

        [HttpPost("Create/{parentId:Guid?}")]
        public async Task<IActionResult> Create(Guid? parentId = null)
        {
            Goal result = await _goalService.CreateAsync(parentId);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
           int deleted =  await _goalService.DeleteAsync(id);
           return Ok(deleted);
        }
    }
}