﻿using Microsoft.AspNetCore.Mvc;

namespace Dmd.TestTask.Web.Components
{
    public class GoalRootTreeComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Views/Home/Components/Goal/GoaltreeRoot.cshtml");
        }
    }
}
