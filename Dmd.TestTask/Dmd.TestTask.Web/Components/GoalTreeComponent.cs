﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Service.Services.Contract;
using Dmd.TestTask.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dmd.TestTask.Web.Components
{
    public class GoalTreeComponent : ViewComponent
    {
        private readonly IGoalService _goalService;

        public GoalTreeComponent(IGoalService goalService)
        {
            _goalService = goalService;
        }

        public async Task<IViewComponentResult> InvokeAsync(ICollection<Goal> goals, bool isFirstCall)
        {
            if (isFirstCall) goals = await _goalService.GetAll();
            GoalViewComponentModel viewModel = new GoalViewComponentModel { IsFirstCall = isFirstCall, Model = goals };
            return View("~/Views/Home/Components/Goal/GoalTree.cshtml", viewModel);
        }
    }
}
