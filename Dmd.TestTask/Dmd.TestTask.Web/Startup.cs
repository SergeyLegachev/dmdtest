﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dmd.TestTask.CommonModule;
using Dmd.TestTask.Dal;
using Dmd.TestTask.Dal.Repository;
using Dmd.TestTask.Service.Exceptions;
using Dmd.TestTask.Service.Services;
using Dmd.TestTask.Service.Services.Contract;
using Dmd.TestTask.Web.Infrastructure.AutoMapper;
using Dmd.TestTask.Web.Infrastructure.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dmd.TestTask.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).
                AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization(); ;
            services.AddDbContext<TestTaskContext>(options =>
                options.UseSqlServer(Configuration.GetSection("ConnectionString").Value));

            services.AddTransient<DbInitialize>();
            services.AddScoped<IGoalRepository, GoalRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IGoalService, GoalService>();
            services.AddSingleton<IProjectionBuilder, ProjectionBuilder>();
            AutoMapperConfiguration.Initialize();
            
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    if (error != null)
                    {
                        var ex = error.Error;
                        if (ex is BusinessRulesException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        }
                        await context.Response.WriteAsync(ex.Message, Encoding.UTF8);
                    }
                });
            });

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
