﻿using AutoMapper;
using Dmd.TestTask.CommonModule;

namespace Dmd.TestTask.Web.Infrastructure.Implementations
{
    public class ProjectionBuilder : IProjectionBuilder
    {
        public TProjection Build<TValue, TProjection>(TValue value)
        {
            var result = Mapper.Map<TValue, TProjection>(value);
            return result;
        }

        public TValue Patch<TValue, TProjection>(TProjection projection, TValue value)
        {
            var result = Mapper.Map(projection, value);
            return result;
        }
    }
}
