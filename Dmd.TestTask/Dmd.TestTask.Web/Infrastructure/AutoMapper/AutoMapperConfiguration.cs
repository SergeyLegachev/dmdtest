﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace Dmd.TestTask.Web.Infrastructure.AutoMapper
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize((cfg) =>
            {
                cfg.AddProfiles(GetAutoMapperProfiles());
            });
        }

        private static IEnumerable<Type> GetAutoMapperProfiles()
        {
            List<Type> result = new List<Type>();
            result.Add(typeof(WebProfile));
            return result;
        }
    }
}
