﻿using System;
using System.Linq;
using AutoMapper;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Dto;
using Dmd.TestTask.Service.Extensions;
using Dmd.TestTask.Web.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Dmd.TestTask.Web.Infrastructure.AutoMapper
{
    public class WebProfile : Profile
    {
        public WebProfile()
        {
            CreateMap<EditTaskDto, GoalEditViewModel>().AfterMap((i, j) =>
            {
                j.AllStatuses = ((Status[]) Enum.GetValues(typeof(Status)))
                    .Select(c => new {Enum = c, Name = c.GetDescription()}).ToList().Select(e => new SelectListItem
                    {
                        Value = j.Status == Status.Assigned && (e.Enum == Status.Finished  || e.Enum == Status.Stopped) ? string.Empty :
                           ((int)e.Enum).ToString(),
                        Text = e.Name
                    }).Where(e=>e.Value != string.Empty);
            });

            CreateMap<EditTaskDto, Goal>().ForMember(e => e.PlanDuration,
                    opt => opt.MapFrom(e => (e.TaskHours * 3600) + (e.TaskMinutes * 60)))
                .ForMember(e => e.FactDuration,
                    opt => opt.MapFrom(e => (e.FactTaskHours * 3600) + (e.FactTaskMinutes * 60)))
                .ForMember(e=>e.Children, opt => opt.Ignore());
        }
    }
}
