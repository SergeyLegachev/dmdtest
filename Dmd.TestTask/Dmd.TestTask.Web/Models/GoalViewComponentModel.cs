﻿using System.Collections.Generic;
using Dmd.TestTask.Domain.Entity;

namespace Dmd.TestTask.Web.Models
{
    public class GoalViewComponentModel
    {
        public bool IsFirstCall { get; set; }
        public ICollection<Goal> Model { get; set; }
    }
}
