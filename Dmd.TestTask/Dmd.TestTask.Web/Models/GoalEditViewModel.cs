﻿using System.Collections.Generic;
using Dmd.TestTask.Domain.Entity;
using Dmd.TestTask.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Dmd.TestTask.Web.Models
{
    public class GoalEditViewModel : EditTaskDto
    {
        public GoalEditViewModel(Goal goal, IEnumerable<ChildGoalDto> children, int childFactDuration, int childPlanDuration) :
            base(goal, children, childFactDuration, childPlanDuration)
        {
        }

        public GoalEditViewModel()
        {
            
        }
        public IEnumerable<SelectListItem> AllStatuses { get; set; }
    }
}
